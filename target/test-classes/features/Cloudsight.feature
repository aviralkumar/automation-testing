Feature: Cloudsight testing

    Scenario: CloudSight testing - Iphone front view
    Given I open "https://cloudsight.ai/" URL
    Then I wait for "5" seconds
    Then I maximize the window
    Then I wait for "9" seconds
    Then I click on button "/html/body/div/div/main/section[1]/a" using xpath as the locator
    Then I wait for "8" seconds
    Then I upload "C:\\Users\\hp\\Desktop\\Gao\\Test2.JPG" image in cloudsight
    Then I wait for "5" seconds
    Then I verify text "IPhone" on the web page
    Then I close the browser

      Scenario: CloudSight testing - Iphone back view
      Given I open "https://cloudsight.ai/" URL
      Then I wait for "5" seconds
      Then I maximize the window
      Then I wait for "9" seconds
      Then I click on button "/html/body/div/div/main/section[1]/a" using xpath as the locator
      Then I wait for "8" seconds
      Then I upload "C:\\Users\\hp\\Desktop\\Gao\\Test3.JPG" image in cloudsight
      Then I wait for "5" seconds
      Then I verify text "IPhone" on the web page

          Scenario: Uploading a File & Verify Blurred Image
          Given I open "https://cloudsight.ai/" URL
          Then I wait for "5" seconds
          Then I maximize the window
          Then I wait for "9" seconds
          Then I click on button "/html/body/div/div/main/section[1]/a" using xpath as the locator
          Then I wait for "8" seconds
          Then I upload "C:\\Users\\hp\\Desktop\\Gao\\Blurred.JPG" image in cloudsight
            Then I wait for "8" seconds


