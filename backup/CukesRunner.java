package cucumber;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.*;
//import cucumber.api.testing.Abs
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources",glue={"MyStepdefs"})

/**
 * Created by hp on 08-04-2017.
 */
public class CukesRunner extends AbstractTestNGCucumberTests {

}
