package com.test.files;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
//import org.apache.bcel.generic.Select;
import net.bytebuddy.implementation.bytecode.Throw;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterTest;
import com.google.transit.realtime.GtfsRealtime.FeedEntity;
import com.google.transit.realtime.GtfsRealtime.FeedMessage;
import java.net.URL;


import java.awt.*;
import java.io.*;
import java.util.*;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.awt.datatransfer.StringSelection;


import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.Calendar;


/*
 * Created by hp on 12-04-2017.
 */
public class MyStepdefs {

    // as there can be only one instance of WebDriver running at single time
    // here we have declared the driver variable as static
    // now all the objects can refer to this

   static WebDriver driver;
    static {

        // set the path using the File class of Java
        File file = new File("C:\\Users\\hp\\driver\\chromedriver.exe");

        if(file.exists())
        {
            System.out.println("File is present in the directory");
        }

        System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

        driver = new ChromeDriver();
    }

    // This is the Static method of the class and it will return the instance of the WebDriver
    // as it is public it can be referred by other classes.

    public static WebDriver getWebDriver()
    {

        return driver;
    }
    @Given("^I generate URL$")
    public void iGenerateURL() {

        // Below is the path where the ChromeDriver.exe is installed
        // creating a file object by passing the file path
        File file = new File("C:\\Users\\hp\\driver\\chromedriver.exe");

        if(file.exists())
        {
            System.out.println("File is present in the directory");
        }

        System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

        WebDriver driver = new ChromeDriver();
          //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        obj.get("http://GOOGLEle.COM");

            driver.get("http://content.icicidirect.com/idirectcontent/Home/Home.aspx");
            driver.manage().timeouts().pageLoadTimeout(8,TimeUnit.SECONDS);
// How do you select a value from the drop down
        // first find out the web element
        // second use the Select Class and pass the Web Element into that class
        // next using the select object use either selectByValue, selectByIndex or selectByVisibleText



            WebElement ele = driver.findElement(By.name("open_account"));
             Select sl = new Select(ele);
            sl.selectByValue("N");
            WebElement link = driver.findElement(By.partialLinkText("Account"));
            WebElement tab = driver.findElement(By.xpath("//*[@id=\"liFE\"]"));
            tab.click();

            link.click();


       // ele2.click();
      //  WebElement ele = obj.findElement(By.className("btn btn-primary"));
      //  ele.click();
        // Get the value of the displayed text and store it in a String for later comparison
      //  String text =  ele.getText();

    //    ele2.click();

      //  String text2 = ele.getText();

       //     Assert.assertFalse(!text.equals(text2));





    }


    @Then("^I click on \"([^\"]*)\" button$")
    public void iClickOnButton(String arg0)  {
        // Write code here that turns the phrase above into concrete actions
       try {

           //*[@id="headerControllerId"]/div[2]/header/div/div/div/div/ul/li[3]/ul/li[2]/a[1]
        //  arg0 = "//[@id=\"headerControllerId\"]/div[2]/header/div/div/div/div/ul/li[3]/ul/li[2]/a[1]";

           driver.findElement(By.xpath(arg0)).click();

       }

       catch(Exception e)

       {

       }
    }

    @Then("^I test ICICI Direct$")
    public void iTestICICIDirect() {


        File file = new File("C:\\Users\\hp\\driver\\chromedriver.exe");

        System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
    // Instantiate the Chrome Driver
        WebDriver driver = new ChromeDriver();

        driver.get("https://secure.icicidirect.com/IDirectTrading/customer/login.aspx");

        // Find the Web Element

        WebElement ele = driver.findElement(By.xpath("//*[@id=\"txtUserId\"]"));
        ele.sendKeys("Testing");
        WebElement ele2 = driver.findElement((By.id("txtPass")));
        ele2.sendKeys("Tes");
        WebElement ele3 = driver.findElement((By.id("txtDOB")));
       ele3.sendKeys("Testing");

        // this is drop downContentPlaceHolder1_drpTrade
      WebElement ele4 = driver.findElement(By.xpath("//*[@id=\"ContentPlaceHolder1_drpTrade\"]"));

        // use the Selct class

        Select s = new Select(ele4);
       s.selectByVisibleText("Currency");
       WebElement ele5 = driver.findElement(By.id("lbtLogin"));
       ele5.click();
        Actions act = new Actions(driver);
        try {
            Robot robot = new Robot();

        }
        catch(Exception e) {

        }
       String actual = driver.findElement(By.id("ContentPlaceHolder1_lblErro")).getText();

      // Assert.assertEquals("Invalid Login Id or Password:Please try agai", actual);
        //Assert.assertTrue(actual.length()==10);
        Assert.assertFalse(actual.length()<10);
        List<WebElement> ele6 = driver.findElements(By.xpath("//*[@class=\"mainNav\"]/ul"));

        for(WebElement el: ele6)
        {
            el.click();

            driver.navigate().back();
        }

        driver.close();



    }

    @Then("^I test NRI Account Opening$")
    public void iTestNRIAccountOpening() {


        // write a script to validate the below
        // Verify all the options listed in the Drop down of opening an Account
        // Select the NRI Account
        // Verify the Text NRI Online Form
        // Select the radio button NRI ( Non Resident Account )
        // Country as United States

        // Selecting the Chrome WEb Driver locati
        File obj = new File("C:\\\\Users\\\\hp\\\\driver\\\\chromedriver.exe");

        // Setting the System Property
        // Instead of setting the property here other option is to set using environment varibales
        System.setProperty("webdriver.chrome.driver",obj.getAbsolutePath());

        // Instantiate the WebDriver

        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);


        driver.get("http://content.icicidirect.com/idirectcontent/Home/Home.aspx");

        // Find out the Id of the Drop down
        // Extract the Web Element
        // to verify the set of values in the drop down with BR
               // System.out.println(e.getStackTrace());



       WebElement ele = driver.findElement(By.id("open_account"));
        // using the Select Class and passing the select drop down into it

        Select st = new Select(ele);
        Assert.assertNotNull(ele);

        // using the getOptions method to get the list of available options in the drop down

        List<WebElement> e = st.getOptions();

        // iterating through all the options one by one by for each loop and putting into arraylist

        ArrayList<String> al = new ArrayList<String>();
        ArrayList<String> al2 = new ArrayList<String>();



        for(WebElement option: e)
        {
            al.add(option.getAttribute("Value"));
        }

        al2.add("S");
        al2.add("N");
        al2.add("A");
        al2.add("C");
        al2.add("U");
        al2.add("F");
        System.out.println(al);
        System.out.println(al2);
       Assert.assertEquals(al2,al);

            // Selecting the NRI Account Value

        st.selectByValue("N");

        WebElement button = driver.findElement((By.className("button3")));
        button.click();
        driver.manage().timeouts().implicitlyWait(4,TimeUnit.SECONDS);

        WebElement rad = driver.findElement((By.id("radUsrType_0")));
        Assert.assertFalse(rad.isSelected());
        rad.click();
        WebElement ele4 = driver.findElement(By.id("USR_ADDRSS_CNTRY"));
        // Pass the Element into the Select cLass

        Select st2 = new Select(ele4);

        st2.selectByValue("ANGOLA");

        driver.findElement(By.id("UNM_PARTB_TNC_FLG")).click();

        driver.findElement((By.id("USR_FRST_NM"))).sendKeys("Aviral");
        driver.findElement(By.id("btnSubmit1")).click();
        driver.quit();





    }

    @Then("^I test icicibank$")
    public void iTestIcicibank() {
        // Write code here that turns the phrase above into concrete actions
        File obj = new File("C:\\\\Users\\\\hp\\\\driver\\\\chromedriver.exe");

        // Setting the System Property
        // Instead of setting the property here other option is to set using environment varibales

        System.setProperty("webdriver.chrome.driver",obj.getAbsolutePath());

        // Instantiate the WebDriver

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.icicibank.com/");
        driver.switchTo().activeElement();

        driver.findElement(By.xpath("//*[@class=\"push-modalContent\"]/div[1]/p[2]/a")).click();

        driver.findElement(By.className("pl-login-ornage-box")).click();
        driver.findElement(By.xpath("//*[@id=\"main\"]/div[2]/div/div/div[1]/div/div[4]/a")).click();
        driver.findElement(By.id("AuthenticationFG.USER_PRINCIPAL")).sendKeys("533015380");
        driver.findElement((By.id("AuthenticationFG.ACCESS_CODE"))).sendKeys("Sanjose@2018");
        driver.findElement((By.id("VALIDATE_CREDENTIALS1"))).click();





    }

    @Then("^I send message to Whatsapp Web$")
    public void  iSendMessageToWhatsappWeb() {
        // Write code here that turns the phrase above into concrete actions
        File obj = new File("C:\\\\Users\\\\hp\\\\driver\\\\chromedriver.exe");

        // Setting the System Property
        // Instead of setting the property here other option is to set using environment varibales
        ArrayList<String> al = new ArrayList<String>();
        al.add("San Jose");
        al.add("Bangalore");
        int count = 0;
        System.setProperty("webdriver.chrome.driver", obj.getAbsolutePath());

            WebDriver driver = MyStepdefs.getWebDriver();
      //   driver = new ChromeDriver();
        // Create object of SimpleDateFormat class and decide the format

        Calendar cal = Calendar.getInstance();

        // print current time
        System.out.println(" Current time is : " + cal.getTime());

        // Print the Date
       // System.out.println("Current date and time is " +date1);

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://web.whatsapp.com/");
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";

        // Get the RequestSpecification of the request that you want to sent
        // to the server. The server is specified by the BaseURI that we have
        // specified in the above step.
        RequestSpecification httpRequest = RestAssured.given();

        // Make a request to the server by specifying the method Type and the method URL.
        // This will return the Response from the server. Store the response in a variable.
        //  System.out.println("Response Body is =>  " + responseBody);
             //  StringBuffer sb = new StringBuffer("I am the Main Thread. All threads originate from me.I dont sleep much. My Sleeping TIme is 25 Seconds. Current Time is :"+cal.getTime());
        driver.findElement(By.id("input-chatlist-search")).sendKeys("Zeeshan Ali");

        int attempts = 0;
        while (attempts < 3) {
            try {
                driver.findElement(By.xpath("//*[@id=\"pane-side\"]/div/div/div/div[4]/div/div/div[2]")).click();



            } catch (Exception e) {
            }
            attempts++;
        }

        for (int i = 0; i < 20; i++) {

            if(count==3)
            {
                count =0;
            }
            Response response = httpRequest.request(Method.GET, "/"+al.get(count));
            count++;

            // Now let us print the body of the message to see what response
            // we have recieved from the server
            String responseBody = response.getBody().asString();
            //
            driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(responseBody);
            driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(Keys.ENTER);
           // sb.append("*Biryani & Kebabs*");
               try{

                    Thread.sleep(100000);


                }

                catch(Exception e)
                {

                }

            //   driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
               //driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys("********Value of I is:"+i);

            //       driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys("*******************Enjoy !!!****************");

          //    driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(Keys.ENTER);

            // System.out.println("Message will be triggered every 2 seconds --- Testing");


            // click on the attachment
        }
        /*

            driver.findElement(By.xpath("//*[@id=\"main\"]/header/div[3]/div/div[2]/div/span")).click();
            try {
                Thread.sleep(1500);
            } catch (Exception e) {

            }

            driver.findElement(By.xpath("//*[@id=\"main\"]/header/div[3]/div/div[2]/span/div/div/ul/li[1]")).click();
            // String sel = new String("C:\\Users\\Desktop\\1.doc");
            StringSelection sel = new StringSelection("C:\\Users\\hp\\Pictures\\Camera Roll\\A");
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel, null);

            // Create object of Robot class

            try {
                Robot robot = new Robot();
                Thread.sleep(8000);
                // Press Enter
                robot.keyPress(KeyEvent.VK_ENTER);

// Release Enter
                robot.keyRelease(KeyEvent.VK_ENTER);

                // Press CTRL+V
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_V);

// Release CTRL+V
                robot.keyRelease(KeyEvent.VK_CONTROL);
                robot.keyRelease(KeyEvent.VK_V);
                Thread.sleep(1000);

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

                driver.findElement((By.xpath("//*[@id=\"app\"]/div/div/div[1]/div[2]/span/div/span/div/div/div[2]/div/span/div/div[2]/div/div[3]/div[1]/div[2]"))).sendKeys(Keys.RETURN);
            } catch (Exception e) {

            }
            */


        }

    @AfterTest
    public void testAdd() {
        String str = "Junit is working fine";
        System.out.println("This is executed after the test");
    }

    @Then("^I print weather data$")
    public void iPrintWeatherData()  {
        // Write code here that turns the phrase above into concrete actions
        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";

        // Get the RequestSpecification of the request that you want to sent
        // to the server. The server is specified by the BaseURI that we have
        // specified in the above step.
        RequestSpecification httpRequest = RestAssured.given();

        // Make a request to the server by specifying the method Type and the method URL.
        // This will return the Response from the server. Store the response in a variable.
        Response response = httpRequest.request(Method.GET, "/Hyderabad");

        // Now let us print the body of the message to see what response
        // we have recieved from the server
        String responseBody = response.getBody().asString();
        System.out.println("Response Body is =>  " + responseBody);


    }

    @Then("^I test DropBox Login$")
    public void iTestDropBoxLogin() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        try {

            //  first step is to set the path for Chrome web driver

            // returns webdriver instance
            WebDriver driver = MyStepdefs.getWebDriver();

            // now using this driver we can perform operations on the web page
            driver.manage().window().maximize();
            driver.navigate().to("https://www.dropbox.com/");
            WebElement ele = driver.findElement(By.name("fname"));
           // driver.findElement(By.linkText("Sign in")).click();
            ele.sendKeys("Aviral");

            WebElement ele2 = driver.findElement(By.xpath("//*[@class=\"login-button button-primary\"]"));


            ele2.click();

            driver.close();





        }

        catch(Throwable e)
        {

        }
    }

    @Then("^I enter text \"([^\"]*)\" in \"([^\"]*)\" box$")
    public void iEnterTextInBox(String text, String locator)  {
        // Write code here that turns the phrase above into concrete actions

        // \ backslash is used as escape character sequemce
        WebDriver driver = MyStepdefs.getWebDriver();
        driver.findElement(By.xpath(locator)).sendKeys(text);

        File file = new File("file path");




    }

    @Given("^I open \"([^\"]*)\" URL$")
    public void iOpenURL(String URL) {
        // Write code here that turns the phrase above into concrete actions
        WebDriver driver;

        driver = MyStepdefs.getWebDriver();
        driver.navigate().to(URL);

    }

    @Then("^I maximize the window$")
    public void iMaximizeTheWindow() {
        // Write code here that turns the phrase above into concrete actions
        driver.manage().window().maximize();
    }

    @Then("^I close the browser$")
    public void iCloseTheBrowser() {
        // Write code here that turns the phrase above into concrete actions
        driver.close();
    }


    @Then("^I press enter key on \"([^\"]*)\" element$")
    public void iPressEnterKeyOnElement(String arg0)  {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.id(arg0)).sendKeys(Keys.ENTER);
    }

    @Then("^I wait for (\\d+) seconds$")
    public void iWaitForSeconds(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I wait for \"([^\"]*)\" seconds$")
    public void iWaitForSeconds(long time) {
        // Write code here that turns the phrase above into concrete actions
        driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
        try {
            Thread.sleep(3000);
        } catch (Exception e) {

        }
    }

    @Then("^I send a message \"([^\"]*)\" to \"([^\"]*)\" xyz$")
    public void iSendAMessageTo(String msg, String name) {
        // Write code here that turns the phrase above into concrete actions
        driver.manage().window().maximize();
        driver.get("https://web.whatsapp.com/");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("input-chatlist-search")).sendKeys(name);

        int attempts = 0;
        while (attempts < 3) {
            try {
                driver.findElement(By.xpath("//*[@id=\"pane-side\"]/div/div/div/div[4]/div/div/div[2]")).click();


            } catch (Exception e) {
            }
            attempts++;
        }

        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(msg);
        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(Keys.ENTER);

        // finding all the elements inside the division

       // List<WebElement> ele = driver.findElements(By.xpath("//*[@class=\"Tkt2p\"]"));
      //  int length = ele.size();
        // System.out.println(ele);
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";

        // Get the RequestSpecification of the request that you want to sent
        // to the server. The server is specified by the BaseURI that we have
        // specified in the above step.
        RequestSpecification httpRequest = RestAssured.given();
       // Response response = httpRequest.request(Method.GET, "/"+al.get(count));


        // Now let us print the body of the message to see what response
        // we have recieved from the server
        String[] s = new String[2];
        String[] s2 = new String[5];
        int length;
        while (1 == 1) {

            List<WebElement> ele = driver.findElements(By.xpath("//*[@class=\"Tkt2p\"]"));
            length = ele.size();
            String input = ele.get(length - 1).getText();

             if(input.contains("Aviral")) {
                int flag = 0;
                if (!input.contains(":")) {
                    driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys("Please Enter in the Format:" + "Aviral:<city name>");
                    flag = 1;
                }
                Response response = httpRequest.request(Method.GET, "/" + "Pune");
                if (flag != 1) {
                    s = input.split(":");

                    try {

                        response = httpRequest.request(Method.GET, "/" + s[1]);
                    } catch (Exception e) {
                        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys("***********MADARCHOD SAHI TYPE KAR*************");
                        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(Keys.ENTER);

                    }
                    String responseBody = response.getBody().asString();
                    s2 = responseBody.split(",");


                    System.out.println("Element that you want is:" + ele.get(length - 1).getText());
                    driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys("***********Weather Report*************");
                    driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(Keys.ENTER);

                    System.out.println("***********Weather Report*************");

                    try {
                        for (int i = 1; i < 5; i++) {
                            driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(s2[i]);
                            driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(Keys.ENTER);
                        }
                    } catch (Exception e) {


                        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys("***********Please Type in the Correct Format: Aviral<City Name>*************");
                        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(Keys.ENTER);

                    }

                    driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys("***********End Of Report*************");
                    driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")).sendKeys(Keys.ENTER);


                }
            }

        }
    }


    @Then("^I click on button \"([^\"]*)\" using xpath as the locator$")
    public void iClickOnButtonUsingXpathAsTheLocator(String arg0)  {
        // Write code here that turns the phrase above into concrete actions
        try {

            driver.findElement(By.xpath(arg0)).click();


           // String sel = new String("C:\\Users\\hp\\Pictures\\Camera Roll\\A.JPG");
           // StringSelection sels = new StringSelection("C:\\Users\\hp\\Pictures\\Camera Roll\\A");
           // Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sels, null);

        }

        catch(Exception e)
        {
            e.printStackTrace();
        }

    }

    @Then("^I click on link with text \"([^\"]*)\"$")
    public void iClickOnLinkWithText(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        try {

            driver.findElement(By.partialLinkText(arg0)).click();

        }

        catch(Throwable e)
        {
           e.printStackTrace();
        }
        throw new PendingException();
    }

    @Then("^I verify text \"([^\"]*)\" on the web page$")
    public void iVerifyTextOnTheWebPage(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        try {

           String body = driver.findElement(By.tagName("body")).getText();

           if(!body.contains(arg0))
           {
               Assert.fail();
           }
            System.out.println(body);

            //Assert.assertEquals(body.contains(arg0),arg0);


        }

        catch(Throwable e)
        {
            Assert.fail();
        }

    }

    @Then("^I click on button with \"([^\"]*)\" id$")
    public void iClickOnButtonWithId(String arg0)  {
        // Write code here that turns the phrase above into concrete actions

        driver.findElement(By.id(arg0)).click();

    }

    @Then("^I upload \"([^\"]*)\" image in cloudsight$")
    public void iUploadImage(String arg0)  {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.id("uploadAction")).sendKeys(arg0);

    }

    @Then("^I click OK on alert box$")
    public void iClickOKOnAlertBox()  {
        // Write code here that turns the phrase above into concrete actions
        driver.switchTo().alert().accept();
    }

    @Then("^I verify text \"([^\"]*)\" on alert box$")
    public void iVerifyTextOnAlertBox(String arg0)  {
        // Write code here that turns the phrase above into concrete actions
        String text = driver.switchTo().alert().getText();

        if(!text.contains(arg0))
        {
           Assert.fail();
        }
    }

    @Then("^I select \"([^\"]*)\" value from the \"([^\"]*)\" dropdown$")
    public void iSelectValueFromTheDropdown(String arg0, String arg1) {
        // Write code here that turns the phrase above into concrete actions
        Select dropdwn = new Select(driver.findElement(By.xpath(arg1)));
        dropdwn.selectByValue(arg0);

    }
}




