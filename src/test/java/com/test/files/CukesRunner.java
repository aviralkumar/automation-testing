package com.test.files;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.*;
//import cucumber.api.te
import cucumber.api.testng.AbstractTestNGCucumberTests;
//@RunWith(Cucumber.class)

/*@CucumberOptions(  monochrome = true,
        tags = "@tags",
        features = "src/test/resources/features",
        format = { "pretty","html: cucumber-html-reports",
                "json: cucumber-html-reports/cucumber.json" },
        dryRun = false,
        glue = "com.visa.files" )
        */
@CucumberOptions( features="src/test/resources/features",format={"json:target/cucumber.json"},glue="com.test.files")
/**
 * Created by hp on 08-04-2017.
 */
public class CukesRunner extends AbstractTestNGCucumberTests {

}
