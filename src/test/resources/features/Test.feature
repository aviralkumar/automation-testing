Feature: Feature file

  Scenario: MY first scenario
    Given I generate URL
    #Then I enter "Bangalore" value in "city-select" textbox
    Then I click on "signin-url" button


    Scenario: Test ICICI Direct

      Then I test ICICI Direct


      Scenario: Test NRI Account Opening

        Then I test NRI Account Opening
        Then I enter text "<string>" in "<string>" box
        Given I open "<string>" URL
        
        
        Scenario: Test Service Now
          Given  I open "https://www.servicenow.com/" URL
          Then I maximize the window
          Then I click on "Search" button
          Then I wait for "3" seconds
          Then I enter text "ITSM" in "gsc-i-id2" box
          Then I press enter key on "gsc-i-id2" element
          Then I wait for "5" seconds
          Then I verify text "ITSM" on the web page
          Then I close the browser
         # Then I close the browser

        Scenario:  Test Login Functionality

          Then I test icicibank

        Scenario: Whats App TEsting

          Then I send message to Whatsapp Web

        Scenario: Weather Test
          Then I print weather data

        Scenario: Testing Whats app
          Then I send a message "Hello" to "Automation" xyz

        Scenario: Order Dominos
          Given I open "https://www.dominos.com/en/" URL
          Then I maximize the window
          Then I wait for "6" seconds
          Then I click on link with text "Order"
          


            Scenario: CloudSight testing - Iphone front view
            Given I open "https://cloudsight.ai/" URL
            Then I wait for "5" seconds
            Then I maximize the window
            Then I wait for "9" seconds
            Then I click on button "/html/body/div/div/main/section[1]/a" using xpath as the locator
            Then I wait for "8" seconds
            Then I upload "C:\\Users\\hp\\Desktop\\Gao\\Test2.JPG" image in cloudsight
            Then I wait for "5" seconds
            Then I verify text "IPhone" on the web page



              Scenario: SurveyApe - Successfull SignIn Testing
              Given I open "http://localhost:3000/" URL
              Then I maximize the window
              Then I click on button "//*[@id='buttonMain']/button" using xpath as the locator
              Then I wait for "2" seconds
              Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[7]/h5/span" using xpath as the locator
              Then I wait for "3" seconds
              Then I enter text "aviralkum@gmail.com" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[2]/div[2]/input" box
              Then I enter text "dollar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[3]/div[2]/input" box
              Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[4]/button" using xpath as the locator
              Then I wait for "4" seconds  
              Then I click OK on alert box
              Then I verify text "Create Survey" on the web page

            Scenario: SurveyApe - Failed SignIn Testing
              Given I open "http://localhost:3000/" URL
              Then I maximize the window
              Then I click on button "//*[@id='buttonMain']/button" using xpath as the locator
              Then I wait for "2" seconds
              Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[7]/h5/span" using xpath as the locator
              Then I wait for "3" seconds
              Then I enter text "aviralkusm@gmail.com" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[2]/div[2]/input" box
              Then I enter text "dollar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[3]/div[2]/input" box
              Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[4]/button" using xpath as the locator
              Then I wait for "4" seconds
              Then I verify text "Failed to register" on alert box

        Scenario: SurveyApe - Registration Testing Successful
      Given I open "http://localhost:3000/" URL
      Then I maximize the window
      Then I click on button "//*[@id='buttonMain']/button" using xpath as the locator
      Then I wait for "2" seconds
      Then I enter text "aviral" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[2]/div[2]/input" box
      Then I enter text "kumar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[3]/div[2]/input" box
      Then I enter text "aviralkum4@gmail.com" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[4]/div[2]/input" box
      Then I enter text "dollar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[5]/div[2]/input" box
      Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[6]/button" using xpath as the locator
      Then I wait for "4" seconds
      Then I verify text "User registration is successful" on alert box
      Then I close the browser

  Scenario: SurveyApe - Registration Testing User Already Exists
    Given I open "http://localhost:3000/" URL
    Then I maximize the window
    Then I click on button "//*[@id='buttonMain']/button" using xpath as the locator
    Then I wait for "2" seconds
    Then I enter text "aviral" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[2]/div[2]/input" box
    Then I enter text "kumar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[3]/div[2]/input" box
    Then I enter text "aviralkum2@gmail.com" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[4]/div[2]/input" box
    Then I enter text "dollar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[5]/div[2]/input" box
    Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[6]/button" using xpath as the locator
    Then I wait for "4" seconds
    Then I verify text "User Already Exists" on alert box
    Then I close the browser


  Scenario: SurveyApe - Redirection to Confirmation Code Page
    Given I open "http://localhost:3000/" URL
    Then I maximize the window
    Then I click on button "//*[@id='buttonMain']/button" using xpath as the locator
    Then I wait for "2" seconds
    Then I enter text "aviral" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[2]/div[2]/input" box
    Then I enter text "kumar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[3]/div[2]/input" box
    Then I enter text "aviralkum8@gmail.com" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[4]/div[2]/input" box
    Then I enter text "dollar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[5]/div[2]/input" box
    Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[6]/button" using xpath as the locator
    Then I wait for "4" seconds
    Then I verify text "User registration is successful" on alert box
    Then I wait for "4" seconds
    Then I click OK on alert box
    Then I verify text "Step Verification" on the web page
    Then I close the browser


  Scenario: SurveyApe - Adding Question to General Survey
    Given I open "http://localhost:3000/" URL
    Then I maximize the window
    Then I click on button "//*[@id='buttonMain']/button" using xpath as the locator
    Then I wait for "2" seconds
    Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[7]/h5/span" using xpath as the locator
    Then I wait for "3" seconds
    Then I enter text "aviralkum@gmail.com" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[2]/div[2]/input" box
    Then I enter text "dollar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[3]/div[2]/input" box
    Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[4]/button" using xpath as the locator
    Then I wait for "4" seconds
    Then I click OK on alert box
    Then I verify text "Create Survey" on the web page
    Then I click on button "//*[@id='root']/div/div/div/div/div[2]/div[1]/div/div/h3" using xpath as the locator
    Then I enter text "TestSurvey" in "//*[@id='root']/div/div/div/div/div/form/div[2]/div[1]/div/input" box
    Then I select "General" value from the "//*[@id='root']/div/div/div/div/div/form/div[2]/div[2]/div/select" dropdown
    Then I select "MCQ" value from the "//*[@id='root']/div/div/div/div/div/form/div[3]/select" dropdown
    Then I select "TEXT" value from the "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[1]/div/select" dropdown
    Then I select "SINGLE" value from the "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[2]/div/select" dropdown
    Then I select "RADIO" value from the "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[3]/div/select" dropdown
    Then I enter text "What is your Gender" in "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[4]/div[1]/div/input" box
    Then I wait for "2" seconds
    Then I click on "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[4]/div[3]/div/input" button
    Then I wait for "10" seconds
    Then I verify text "What is your Gender" on the web page
   #Then I close the browser

  Scenario: SurveyApe - Adding Question to Closed Survey
    Given I open "http://localhost:3000/" URL
    Then I maximize the window
    Then I click on button "//*[@id='buttonMain']/button" using xpath as the locator
    Then I wait for "2" seconds
    Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[7]/h5/span" using xpath as the locator
    Then I wait for "3" seconds
    Then I enter text "aviralkum@gmail.com" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[2]/div[2]/input" box
    Then I enter text "dollar" in "//*[@id='root']/div/div/div/div/div/div/div[2]/div[3]/div[2]/input" box
    Then I click on button "//*[@id='root']/div/div/div/div/div/div/div[2]/div[4]/button" using xpath as the locator
    Then I wait for "4" seconds
    Then I click OK on alert box
    Then I verify text "Create Survey" on the web page
    Then I click on button "//*[@id='root']/div/div/div/div/div[2]/div[1]/div/div/h3" using xpath as the locator
    Then I enter text "TestSurvey" in "//*[@id='root']/div/div/div/div/div/form/div[2]/div[1]/div/input" box
    Then I select "Closed" value from the "//*[@id='root']/div/div/div/div/div/form/div[2]/div[2]/div/select" dropdown
    Then I select "MCQ" value from the "//*[@id='root']/div/div/div/div/div/form/div[3]/select" dropdown
    Then I select "TEXT" value from the "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[1]/div/select" dropdown
    Then I select "SINGLE" value from the "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[2]/div/select" dropdown
    Then I select "RADIO" value from the "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[3]/div/select" dropdown
    Then I enter text "What is your Gender" in "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[4]/div[1]/div/input" box
    Then I wait for "2" seconds
    Then I click on "//*[@id='root']/div/div/div/div/div/form/div[4]/div/div/div[4]/div[3]/div/input" button
    Then I wait for "10" seconds
    Then I verify text "What is your Gender" on the web page








    #User Registration is successful






            






